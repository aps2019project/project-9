package model.specialPower;

public enum OnSpawnTargetCell{
    EIGHT_AROUND,
    A_RANDOM_ENEMY_MINION,
    TWO_DISTANCE_CELLS,
    ALL_FRIENDLY_MINIONS,
}
