package model.enumerations;

public enum SpecialPowerActivationTime {
    NONE,
    ON_SPAWN,
    PASSIVE,
    ON_DEATH,
    ON_ATTACK,
    ON_DEFEND,
    COMBO,
}
