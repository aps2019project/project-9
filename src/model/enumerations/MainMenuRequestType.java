package model.enumerations;

public enum MainMenuRequestType {
    COLLECTION,
    SHOP,
    BATTLE,
    EXIT,
    HELP
}
