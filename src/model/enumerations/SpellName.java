package model.enumerations;

public enum SpellName {
    TOTAL_DISARM,
    AREA_DISPEL,
    EMPOWER,
    FIREBALL,
    GOD_STRENGTH,
    HELLFIRE,
    LIGHTING_BOLT,
    POISON_LAKE,
    MADNESS,
    ALL_DISARM,
    ALL_POISON,
    DISPEL,
    HEALTH_WITH_PROFIT,
    POWER_UP,
    ALL_POWER,
    ALL_ATTACK,
    WEAKENING,
    SACRIFICE,
    KINGS_GUARD,
    SHOCK,
    CUSTOM
}
