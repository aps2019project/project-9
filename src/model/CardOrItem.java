package model;

public abstract class CardOrItem {
    public abstract String getName();
    public abstract int getCardID();
}
