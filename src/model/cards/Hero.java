package model.cards;

import com.google.gson.annotations.Expose;
import model.Cell;
import model.buffs.Buff;
import model.cellaffects.CellAffect;
import model.enumerations.CardType;
import model.enumerations.HeroName;
import model.enumerations.MinionAttackType;
import model.specialPower.SpecialPower;

import java.util.ArrayList;

public class Hero extends Minion {
    // hero does not have minion name ( not use the minion name that inherit )
    // for hero MP is the buffs cost
    @Expose
    private int coolDown;
    private int turnsRemained; // for cool down
    @Expose
    private ArrayList<Buff> buffs;
    @Expose
    private CellAffect cellAffect;
    @Expose
    private HeroTargetType buffsTargetType; // for spell
    @Expose
    private HeroName heroName;
    private boolean isSpecialPowerActivated = false;
    private Spell spell = null;

    public Hero(HeroName heroName, int cost, int HP, int AP, MinionAttackType attackType, int attackRange,
                ArrayList<Buff> buffs, int MP, int coolDown, int cardID, String name, String desc, boolean isFars,
                HeroTargetType buffsTargetType, CellAffect cellAffect) {
        super(name, cost, MP, HP, AP, attackType, attackRange, null, CardType.MINION, cardID, desc,
                null, isFars); // MP is for special power in hero ( spell )
        this.coolDown = coolDown;
        this.buffs = buffs;
        this.buffsTargetType = buffsTargetType;
        this.heroName = heroName;
        this.cellAffect = cellAffect;
    }

    public Hero(Spell spell,HeroName heroName, int cost, int HP, int AP, MinionAttackType attackType, int attackRange,
                int MP, int coolDown, int cardID, String name, String desc, boolean isFars) {
        super(name, cost, MP, HP, AP, attackType, attackRange, null, CardType.MINION, cardID, desc,
                null, isFars); // MP is for special power in hero ( spell )
        this.coolDown = coolDown;
        this.spell = spell;
        this.heroName = heroName;
    }

    public Spell getSpell() {
        return spell;
    }

    public int getCoolDown() {
        return coolDown;
    }

    public boolean isSpellReady() {
        return turnsRemained == 0;
    }

    public void useSpecialPower(Cell cell) {// cast spell
        turnsRemained = coolDown;
        if (spell == null) {
            if (buffsTargetType == HeroTargetType.ON_ATTACK) {
                if (isSpecialPowerActivated) {
                    for (Buff buff : buffs) {
                        buff.getCopy().startBuff(cell);
                    }
                } else
                    isSpecialPowerActivated = true;
            } else if (buffsTargetType == HeroTargetType.ITSELF) {
                if (buffs != null) {
                    for (Buff buff : buffs) {
                        buff.getCopy().startBuff(getCell());
                    }
                }
            } else if (buffsTargetType == HeroTargetType.ALL_POWERS_IN_ROW) {
                if (buffs != null) {
                    for (Cell cell1 : player.getBattle().getPlayGround().enemyInRow(getCell(), player.getOpponent())) {
                        for (Buff buff : buffs) {
                            buff.getCopy().startBuff(cell1);
                        }
                    }
                }
            } else if (buffsTargetType == HeroTargetType.AN_ENEMY_POWER) {
                if (buffs != null) {
                    Cell target = player.getBattle().getPlayGround().getRandomPowerCell(player);
                    for (Buff buff : buffs) {
                        buff.getCopy().startBuff(target);
                    }
                    player.addMana(-MP);
                } else if (heroName == HeroName.AFSANE) {
                    Cell target = player.getBattle().getPlayGround().getRandomPowerCell(player.getOpponent());
                    target.getMinionOnIt().dispelPositiveBuffs();
                }
            } else if (buffsTargetType == HeroTargetType.A_CELL) {
                if (cellAffect != null) {
                    Cell target = player.getBattle().getPlayGround().getRandomCell();
                    cellAffect.putCellAffect(target);
                }
            } else if (buffsTargetType == HeroTargetType.ALL_ENEMY_POWERS) {
                if (buffs != null) {
                    for (Minion minion : player.getOpponent().getMinionsInPlayGround()) {
                        for (Buff buff : buffs) {
                            buff.getCopy().startBuff(minion.getCell());
                        }
                    }
                }
            }
            player.addMana(-MP);
        }else{
            spell.castSpell(SpecialPower.getSpellCastCell(spell.getTargetType(),this));
            player.addMana(-spell.MP);
        }
    }

    public HeroTargetType getBuffsTargetType() {
        return buffsTargetType;
    }

    public String toString() {
        String string = "Name : " + getName() + "\nAP : " + getAP() + "\nHP : " + getHP() + "\nClass : "
                + getAttackType() + "\nSpecial power: " + getDesc() + "\ncost : " + getCost();
        return string;
    }

    public ArrayList<Buff> getBuffs() {
        return buffs;
    }

    public void setTurnsRemainedForNextTurn() {// every turn ( in nextTurn() )
        if (turnsRemained != 0) {
            turnsRemained--;
        }
    }

    public int getTurnsRemained() {
        return turnsRemained;
    }

    public HeroName getHeroName() {
        return heroName;
    }

    public boolean isSpecialPowerActivated() {
        return isSpecialPowerActivated;
    }

    public void setBuffs(ArrayList<Buff> buffs) {
        this.buffs = buffs;
    }

    public void setCellAffect(CellAffect cellAffect) {
        this.cellAffect = cellAffect;
    }

    public CellAffect getCellAffect() {
        return cellAffect;
    }
}
