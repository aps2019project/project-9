package model.cards;

public enum HeroTargetType {
    ITSELF,
    ALL_ENEMY_POWERS,
    AN_ENEMY_POWER,
    ON_ATTACK,
    A_CELL,
    ALL_POWERS_IN_ROW,
}
