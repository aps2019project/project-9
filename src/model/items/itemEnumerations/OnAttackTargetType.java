package model.items.itemEnumerations;

public enum OnAttackTargetType {
    OPPONENT_CELL, // in attack()
    RANDOM_ENEMY,
}
