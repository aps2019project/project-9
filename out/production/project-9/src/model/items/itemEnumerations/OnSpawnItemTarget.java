package model.items.itemEnumerations;

public enum OnSpawnItemTarget {
    ENEMY_HERO,
    EVERY_MINION
}
