package model.enumerations;

public enum InGameRequestType {
    GAME_INFO,
    SHOW_MY_MINIONS,
    SHOW_OPPONENT_MINIONS,
    SHOW_CARD_INFO,
    SELECT_CARD,
    MOVE_TO,
    ATTACK,
    COMBO_ATTACK,
    USE_SPECIAL_POWER,
    SHOW_HAND,
    INSERT,
    END_TURN,
    SHOW_COLLECTIBLES,
    SELECT_ITEM,
    SHOW_INFO,
    USE,
    SHOW_NEXT_CARD,
    ENTER_GRAVEYARD,
    HELP,
    EXIT,
    END_GAME,
    SHOW_MENU,
}
