package model.enumerations;

public enum BuffName {
    HOLLY,
    POWER,
    POISON,
    WEAKNESS,
    STUN,
    DISARM,
    DELAY,
}
