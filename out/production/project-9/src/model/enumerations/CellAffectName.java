package model.enumerations;

public enum CellAffectName {
    POISON,
    FIRE,
    HOLLY
}
